#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	////la taille du tableau est egal au nombre de caractère initialisés
	//char chaine1[] = "Hello Chaine 1";
	//std::cout << "--> chaine 1 = " << chaine1 << std::endl;

	////initialisé a 15
	//char chaine2[15] = "Hello Chaine 2";
	//std::cout << "--> chaine 2 = " << chaine2 << std::endl;
	////0 = null, donc si on a un zero dans la chaine, ça stop la chaine
	//chaine2[5] = 0;
	//std::cout << "--> chaine 2 = " << chaine2 << std::endl;

	////
	//const char* chaine3 = "hello chaine 3";
	//std::cout << "--> chaine 3 = " << chaine3 << std::endl;

	////on rentre une chaine de - de 15 caractères dans un tableau de 15 caractères, dans les cases vident, la premiere contient la valeur null
	////cases d'une nouvelle saisie non utilisés gardent la meme valeur que la premiere saisie
	//cout << "saisie dune chaine de 14 caracteres max: " << endl;
	//cin >> chaine2;
	//chaine2[7] = 'A';
	//std::cout << "--> chaine2 = " << chaine2 << std::endl;
	////on dépasse les 15, on a une erreur
	//cout << "saisie d'une chaine de plus de 14 caracteres:" << endl;
	//cin >> chaine2;
	//std::cout << "--> chaine2 = " << chaine2 << std::endl;


}

//faire RotationDroite(int* tabOrg, int lentab, int* tabDest, int nbRot=1)

double Moyenne(int tab[]/*int* tab*/, int lentab)
{
	double moyenne, total = 0;
	for (int i = 0; i < lentab - 1; i++)
	{
		total = total + tab[i];
	}
	moyenne = total / lentab;
	return moyenne;
}
